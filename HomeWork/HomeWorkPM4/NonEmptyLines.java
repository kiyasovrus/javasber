package HomeWorkPM4;

import java.util.ArrayList;
import java.util.List;

public class NonEmptyLines {
    public static void main(String[] args) {
        List<String> elements = new ArrayList<>(List.of("abc", "", "", "def", "qqq"));
        System.out.println(elements.stream()
                .filter(s -> !(s.equals("")))
                .count());
    }
}
