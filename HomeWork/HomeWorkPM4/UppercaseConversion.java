package HomeWorkPM4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UppercaseConversion {
    public static void main(String[] args) {
        List<String> elements = List.of("abc", "def", "XYz", "qqq");
        String result = elements.stream()
                .map(s -> s.toUpperCase())
                .collect(Collectors.joining(", "));
        System.out.println(result);
    }
}
