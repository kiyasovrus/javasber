package HomeWorkPM4;

import java.util.Collections;
import java.util.List;

public class Descending {
    public static void main(String[] args) {
        List<Double> numbers = List.of(5.1, 3.2, 2.2, 4.7, 1.1, 2.2);
        numbers.stream()
                .sorted(Collections.reverseOrder())
                .forEach(x -> System.out.print(x + " "));
    }
}
