package homework_filmlibrary_11.src.main.java.ru.sbercourse.filmlibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilmWithDirectorsDto extends FilmDto{

  private List<DirectorDto> directors;
}
