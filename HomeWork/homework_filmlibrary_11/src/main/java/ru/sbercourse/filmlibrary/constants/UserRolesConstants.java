package homework_filmlibrary_11.src.main.java.ru.sbercourse.filmlibrary.constants;

public interface UserRolesConstants {
  String ADMIN = "ADMIN";
  String MANAGER = "MANAGER";
  String USER = "USER";

}
