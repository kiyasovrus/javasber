package homework_filmlibrary_11.src.main.java.ru.sbercourse.filmlibrary.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

  @GetMapping("/")
  public String index() {
    return "index";
  }
}
