package HomeWork2;

import java.util.Scanner;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.
После этого вводится число X — элемент, который нужно добавить в массив, чтобы сортировка в массиве сохранилась.


Необходимо вывести на экран индекс элемента массива, куда нужно добавить X.
Если в массиве уже есть число равное X, то X нужно поставить после уже существующего.

 */

public class Hw2Part1Task3 {
//    public static void main(String[] args) {
//        Scanner console = new Scanner(System.in);
//        int n = console.nextInt();
//        int[] firstArray = new int[n];
//        for (int i = 0; i < firstArray.length; i++)
//            firstArray[i] = console.nextInt();
//        int x = console.nextInt();
//        System.out.println(Arrays.binarySearch(firstArray,x) - 1);


//    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++)
            arr[i] = sc.nextInt();
        int x = sc.nextInt();

        int result = n;
        for (int i = 0; i <= arr.length; i++) {
            result = i;
            if (i < arr.length && arr[i] > x)
                break;
        }

        System.out.println(result);
        sc.close();
    }
}