package HomeWork2;

import java.util.Scanner;

//На вход подается строка S, состоящая только из русских заглавных букв (без Ё).
//
//        Необходимо реализовать метод, который кодирует переданную строку с помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв нужно пробелом.
//
//        Для удобства представлен массив с кодами Морзе ниже:
//        {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"}


public class Hw2Part1Task6 {
    public static void main(String[] args) {
        String string;

        Scanner scanner = new Scanner(System.in);
        string = scanner.nextLine();

        System.out.println(morseCode(string));



    }

    public static String morseCode(String s) {
        String result = "";
        String[] morse = new String[]{".-", "-...", ".--", "--.", "-..", ".", "...-",
                "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
                "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--",
                "-.--", "-..-", "..-..", "..--", ".-.-"};
        char[] alphabet = new char[]{'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О',
                'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};

        char[] stringToChar = s.toCharArray();

        for (int i = 0; i < stringToChar.length; i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (stringToChar[i] == alphabet[j]) {
                    result+=morse[j] + " ";
                }
            }
        }
        return result;
    }
}
