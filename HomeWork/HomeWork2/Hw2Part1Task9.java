package HomeWork2;

import java.util.Objects;
import java.util.Scanner;
/*
На вход подается число N — длина массива.
        Затем передается массив строк из N элементов (разделение через перевод строки).
        Каждая строка содержит только строчные символы латинского алфавита.

        Необходимо найти и вывести дубликат на экран.
        Гарантируется что он есть и только один.
*/

public class Hw2Part1Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        String[] arr = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = console.next();
        }


        String word = "";

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (Objects.equals(arr[i],arr[j])){
                    word = arr[i];
                }
            }
        }
        System.out.println(word);
    }
}
