package HomeWork2;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного, возведением в квадрат каждого элемента, упорядочить элементы по возрастанию и вывести их на экран.
*/

public class Hw2Part1Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = console.nextInt();
            arr[i] = arr[i] * arr[i];
        }

        int[] sortArray = new int[arr.length];
        int pos = 0;

        for (int element : arr) {
            sortArray[pos] = element;
            pos++;
        }

        Arrays.sort(arr);


        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
