package HomeWork2;

import java.util.Scanner;

//Петя решил начать следить за своей фигурой.
//        Но все существующие приложения для подсчета калорий ему не понравились и он решил написать свое.
//        Петя хочет каждый день записывать сколько белков, жиров, углеводов и калорий он съел, а в конце недели приложение должно его уведомлять, вписался ли он в свою норму или нет.
//
//        На вход подаются числа A — недельная норма белков, B — недельная норма жиров, C — недельная норма углеводов и K — недельная норма калорий.
//        Затем передаются 7 строк, в которых в том же порядке указаны сколько было съедено Петей нутриентов в каждый день недели.
//        Если за неделю в сумме по каждому нутриенту не превышена недельная норма, то вывести “Отлично”, иначе вывести “Нужно есть поменьше”.

public class Hw2Part2Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isOk = true;
        int[] norm = new int[4];
        int[] week = new int[4];

        for (int i = 0; i < norm.length; i++) {
            norm[i] = scanner.nextInt();
        }

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < week.length; j++) {
                week[j] += scanner.nextInt();
            }
        }

        for (int i = 0; i < norm.length; i++) {
            if (week[i] > norm[i]) isOk = false;
        }

        String answer = isOk == true ? "Отлично" : "Нужно есть поменьше";
        System.out.println(answer);
    }
}
