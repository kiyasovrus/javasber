package HomeWork2;

import java.util.Scanner;

//        На вход подается число N.
//        Необходимо посчитать и вывести на экран сумму его цифр.
//        Решить задачу нужно через рекурсию.

public class Hw2Part2Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(recursiveSum(scanner.nextInt()));
    }

    static int recursiveSum (int i) {
        if (i < 10) return i;
        return i % 10 + recursiveSum(i/10);
    }
}
