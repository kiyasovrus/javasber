package HomeWork2;

import java.util.Scanner;

public class Hw2Part2Task4 {


        public static void main(String[] args) {

            Scanner sc = new Scanner(System.in);
            int n = sc.nextInt();

            int[][] arr = new int[n][n];
            for (int i = 0; i < arr.length; i++)
                for (int j = 0; j < arr[i].length; j++)
                    arr[i][j] = sc.nextInt();

            int p = sc.nextInt();

            int id = -1;
            int jd = -1;
            for (int i = 0; i < arr.length; i++)
                for (int j = 0; j < arr[i].length; j++)
                    if (arr[i][j] == p) {
                        id = i;
                        jd = j;
                    }

            int[][] res = new int[n - 1][n - 1];
            for (int i = 0; i < res.length; i++)
                for (int j = 0; j < res[i].length; j++)
                    res[i][j] = arr[(i < id) ? i : i + 1][(j < jd) ? j : j + 1];

            for (int i = 0; i < res.length; i++) {
                for (int j = 0; j < res[i].length; j++)
                    System.out.print(((j == 0) ? "" : " ") + res[i][j]);
                System.out.println();
            }

            sc.close();
        }
    }

