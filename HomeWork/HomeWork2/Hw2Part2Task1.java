package HomeWork2;

import java.util.Scanner;

//        На вход передается N — количество столбцов в двумерном массиве и M — количество строк.
//        Затем сам передается двумерный массив, состоящий из натуральных чисел.
//
//        Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.


public class Hw2Part2Task1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int columnAmount, rowAmount;
        columnAmount = scanner.nextInt();
        rowAmount = scanner.nextInt();
        int[][] array = new int[rowAmount][columnAmount];

        int[] min = new int[rowAmount];
        for (int i = 0; i < rowAmount; i++) {
            min[i] = Integer.MAX_VALUE;
        }
        for (int i = 0; i < rowAmount; i++) {
            array[i] = new int[columnAmount];
            for (int j = 0; j < columnAmount; j++) {
                array[i][j] = scanner.nextInt();
                if (array[i][j] < min[i]) min[i] = array[i][j];
            }
        }
        for (int i = 0; i < rowAmount; i++) {
            System.out.print(min[i] + " ");
        }
    }
}

