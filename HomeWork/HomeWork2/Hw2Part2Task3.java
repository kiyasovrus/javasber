package HomeWork2;

import java.util.Scanner;

//На вход подается число N — количество строк и столбцов матрицы. Затем передаются координаты X и Y расположения коня на шахматной доске.
//
//        Необходимо заполнить матрицу размера NxN нулями, местоположение коня отметить символом K, а позиции, которые он может бить, символом X.

public class Hw2Part2Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x, y, matrixSize;
        matrixSize = scanner.nextInt();
        String[][] matrix = new String[matrixSize][matrixSize];
        x = scanner.nextInt();
        y = scanner.nextInt();

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                matrix[i][j] = "0";
                if (i == y && j == x) {
                    matrix[i][j] = "K";
                }
                if ((Math.abs(j - x) == 1 && Math.abs(i - y) == 2) || (Math.abs(j - x) == 2 && Math.abs(i - y) == 1)) {
                    matrix[i][j] = "X";
                }
                if (j == matrixSize-1) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.print(matrix[i][j] + " ");
                }

            }
            System.out.println("");
        }
    }
}
