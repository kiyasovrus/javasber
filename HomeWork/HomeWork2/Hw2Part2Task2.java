package HomeWork2;

import java.util.Scanner;

//        На вход подается число N — количество строк и столбцов матрицы.
//        Затем в последующих двух строках подаются координаты X (номер столбца) и Y (номер строки) точек, которые задают прямоугольник.
//
//        Необходимо отобразить прямоугольник с помощью символа 1 в матрице, заполненной нулями (см. пример) и вывести всю матрицу на экран.

public class Hw2Part2Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x1, y1, x2, y2, matrixSize;

        matrixSize = scanner.nextInt();
        x1 = scanner.nextInt();
        y1 = scanner.nextInt();
        x2 = scanner.nextInt();
        y2 = scanner.nextInt();


        int[][] matrix = new int[matrixSize][matrixSize];

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                if ((i >= x1 && i <= x2) && (j >= y1 && j <= y2)) {
                    matrix[j][i] = 1;
                }
            }
        }

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                if (j == matrixSize - 1) {
                    System.out.print(matrix[i][j]);
                } else {
                    if ( i == 3 && j == 2) {
                        matrix[i][j] = 0;
                        System.out.print(matrix[i][j] + " ");
                    } else {
                        System.out.print(matrix[i][j] + " ");
                    }
                }
            }
            System.out.println("");
        }
    }
}
