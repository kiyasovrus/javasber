package HomeWork2;

import java.util.Scanner;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M — величина сдвига.

Необходимо циклически сдвинуть элементы массива на M элементов вправо.
 */

public class Hw2Part1Task5 {
    public static void main(String[] args) {

        int n, s;
        int[] arrayFirst;

        Scanner console = new Scanner(System.in);

        n = console.nextInt();
        arrayFirst = new int[n];


        for (int i = 0; i < arrayFirst.length; i++) {
            arrayFirst[i] = console.nextInt();
        }

        s = console.nextInt();

        int[] arraySecond = new int[arrayFirst.length];
        for (int i = 0; i < arrayFirst.length; i++) {
            if ((i + s) < arrayFirst.length) {
                arraySecond[i + s] = arrayFirst[i];
            } else {
                arraySecond[(i + s) - arrayFirst.length] = arrayFirst[i];
            }

        }
        for (int i = 0; i < arraySecond.length; i++)
            System.out.print(arraySecond[i] + " ");

    }
}
