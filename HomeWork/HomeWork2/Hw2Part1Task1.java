package HomeWork2;

import java.util.Scanner;

/*
1. Посчитать среднее арифметическое
На вход подается число N — длина массива.
Затем передается массив вещественных чисел (ai) из N элементов.


Необходимо реализовать метод, который принимает на вход полученный массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
 */

public class Hw2Part1Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        double[] myList = new double[n];
        //System.out.print("Введите " + myList.length + " значений: ");
        for (int i = 0; i < myList.length; i++)
            myList[i] = console.nextDouble();
        double total = 0;
        for (int i = 0; i < myList.length; i++) {
            total += myList[i];
        }
        System.out.println(total / n);

    }

}


