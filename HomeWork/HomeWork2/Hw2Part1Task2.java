package HomeWork2;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого аналогично передается второй массив (aj) длины M.

Необходимо вывести на экран true, если два массива одинаковы (то есть содержат одинаковое количество элементов и для каждого i == j элемент ai == aj). Иначе вывести false.
 */

public class Hw2Part1Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] firstArray = new int[n];
        for (int i = 0; i < firstArray.length; i++)
            firstArray[i] = console.nextInt();
        int m = console.nextInt();
        int[] secondArray = new int[m];
        for (int i = 0; i < secondArray.length; i++)
            secondArray[i] = console.nextInt();
        if (Arrays.equals(firstArray,secondArray))
            System.out.println(true);
        else
            System.out.println(false);


    }

}