package HomeWork2;

import java.util.Scanner;

//На вход подается число N — количество строк и столбцов матрицы.
//        Затем передается сама матрица, состоящая из натуральных чисел.
//
//        Необходимо вывести true, если она является симметричной относительно побочной диагонали, false иначе.
//
//        Побочной диагональю называется диагональ, проходящая из верхнего правого угла в левый нижний.

public class Hw2Part2Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        boolean isSymmetric = true;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] != matrix[n - 1 - j][n - 1 - i]) {
                    isSymmetric = false;
                }
            }
        }
        System.out.println(isSymmetric);
    }
}

