package HomeWork2;

import java.util.Scanner;

//Раз в год Петя проводит конкурс красоты для собак.
//        К сожалению, система хранения участников и оценок неудобная, а победителя определить надо.
//        В первой таблице в системе хранятся имена хозяев, во второй - клички животных, в третьей — оценки трех судей за выступление каждой собаки.
//        Таблицы связаны между собой только по индексу.
//        То есть хозяин i-ой собаки указан в i-ой строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы.
//        Нужно помочь Пете определить топ 3 победителей конкурса.
//
//        На вход подается число N — количество участников конкурса. Затем в N строках переданы имена хозяев.
//        После этого в N строках переданы клички собак.
//        Затем передается матрица с N строк, 3 вещественных числа в каждой — оценки судей.
//        Победителями являются три участника, набравшие максимальное среднее арифметическое по оценкам 3 судей.
//        Необходимо вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
//        Среднюю оценку выводить с точностью один знак после запятой.
//
//        Гарантируется, что среднее арифметическое для всех участников будет различным.


public class Hw2Part2Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int matrixSize = scanner.nextInt();
        String[] dogOwners = new String[matrixSize];
        String[] dogs = new String[matrixSize];
        double[][] scores = new double[matrixSize][3];
        double[] totalScores = new double[matrixSize];
        scanner.nextLine();
        for (int i = 0; i < matrixSize; i++) {
            dogOwners[i] = scanner.nextLine();
        }
        for (int i = 0; i < matrixSize; i++) {
            dogs[i] = scanner.nextLine();
        }


        for (int i = 0; i < scores.length; i++) {
            for (int j = 0; j < 3; j++) {
                scores[i][j] = scanner.nextDouble();
                totalScores[i] += scores[i][j];
            }
        }

        for (int i = 0; i < totalScores.length; i++) {
            totalScores[i] /= 3.0;
        }

        int[] medalPlaces = new int[] {-1,-1,-1};
        double max;
        int count = 0;

        for (int i = 0; i < matrixSize; i++) {
            if (count == 3) break;
            max = Double.MIN_VALUE;
            for (int j = 0; j < matrixSize; j++) {
                if (j == medalPlaces[0] || j == medalPlaces[1] || j == medalPlaces[2]) continue;
                if (totalScores[j] > max) {
                    max = totalScores[j];
                    medalPlaces[count] = j;
                }
            }
            count++;

        }

        for (int i = 0; i < medalPlaces.length; i++) {
            System.out.println(dogOwners[medalPlaces[i]] + ": " + dogs[medalPlaces[i]] + ", " + Math.floor(totalScores[medalPlaces[i]] * 10) / 10);
        }

    }
}


