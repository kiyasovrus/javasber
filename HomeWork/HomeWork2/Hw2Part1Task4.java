package HomeWork2;

import java.util.Scanner;
/*
На вход подается число N — длина массива.
        Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

        Необходимо вывести на экран построчно сколько встретилось различных элементов.
        Каждая строка должна содержать количество элементов и сам элемент через пробел.
*/
public class Hw2Part1Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = console.nextInt();
        }

        int count = 0;
        int value = arr[0];
        for (int i = 0; i < n; i++) {
            if (arr[i] == value) {
                count++;
            } else {
                System.out.println(count + " " + value);
                count = 1;
                value = arr[i];
            }
        }
        System.out.println(count + " " + value);
    }
}
