package HomeWork2;

import java.util.Scanner;

public class Hw2Part1Task8 {
    public static void main(String[] args) {
        int m, difference, closerValueIndex;
        int[] array;

        Scanner scanner = new Scanner(System.in);
        array = new int[scanner.nextInt()]; // вводим количество элементов массива

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt(); // вводим элементы массива
        }

        m = scanner.nextInt();

        difference = Integer.MAX_VALUE;
        closerValueIndex = -1;

        for (int i = 0; i < array.length; i++) {
            if ((Math.abs(array[i] - m)) < difference) {
                closerValueIndex = i;
            }
        }

        System.out.println(array[closerValueIndex]);


    }
}
