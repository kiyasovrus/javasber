package HomeWork2;

import java.util.Scanner;

// На вход подается число N.
// Необходимо вывести цифры числа справа налево.
// Решить задачу нужно через рекурсию.


public class Hw2Part2Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        recursiveOut(n);

    }

    static void recursiveOut(int i) {
        if (i < 10) {
            System.out.print(i + " ");
            return;
        }
        System.out.print(i%10 + " ");
        recursiveOut(i/10);
    }
}
