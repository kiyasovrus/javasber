package HomeWork2;

import java.util.Scanner;

//        На вход подается число N.
//        Необходимо вывести цифры числа слева направо.
//        Решить задачу нужно через рекурсию.

public class Hw2Part2Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        recursiveSum(n);
    }
    static void recursiveSum(int i) {
        if (i < 10) {
        } else {
            recursiveSum(i / 10);
        }
        System.out.print(i%10 + " ");
    }
}
