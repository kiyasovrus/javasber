package homework_filmlibrary_12.src.main.java.ru.sbercourse.filmlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmLibraryApp {

    public static void main(String[] args) {
        SpringApplication.run(FilmLibraryApp.class, args);
    }
}
