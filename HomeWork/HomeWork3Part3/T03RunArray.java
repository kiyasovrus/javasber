package HomeWork3Part3;

import java.util.ArrayList;
import java.util.Scanner;

public class T03RunArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> tempArray = new ArrayList<>();
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        for (int i = 0; i < m; i++) {
            tempArray.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                tempArray.get(i).add(i + j);
            }
        }

        for (ArrayList<Integer> integers : tempArray) {
            for (Integer integer : integers) {
                System.out.print(integer + " ");
            }
            System.out.println();
        }
    }
}
