package HomeWork3Part3;

public class T01RunWildlife {
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.eat();
        bat.sleep();
        bat.fly();
        bat.wayOfBirth();
        System.out.println("\n");
        Dolphin dolphin = new Dolphin();
        dolphin.eat();
        dolphin.sleep();
        dolphin.swim();
        dolphin.wayOfBirth();
        System.out.println("\n");
        GoldFish goldFish = new GoldFish();
        goldFish.eat();
        goldFish.sleep();
        goldFish.swim();
        goldFish.wayOfBirth();
        System.out.println("\n");
        Eagle eagle = new Eagle();
        eagle.eat();
        eagle.sleep();
        eagle.fly();
        eagle.wayOfBirth();
    }
}

abstract class Animal {
    protected final void eat() {
        System.out.println("Все питаются, исключений нет");
    }
    protected final void sleep() {
        System.out.println("Все спят, без исключений");
    }
}

interface Bird {
    default void wayOfBirth() {
        System.out.println("Птицы откладывают яйца");
    }
}

interface Mammal {
    default void wayOfBirth() {
        System.out.println("Живородящие");
    }
}

interface Fish {
    default void wayOfBirth() {
        System.out.println("Рыбы мечут икру");
    }
}

interface FlyingAnimal {
    public void fly();
}

interface SwimmingAnimal {
    public void swim();
}

class Bat extends Animal implements FlyingAnimal, Mammal{
    @Override
    public void fly() {
        System.out.println("Летучая мышь умеет летать");
    }
}

class Dolphin extends Animal implements SwimmingAnimal, Mammal{
    @Override
    public void swim() {
        System.out.println("Дельфин умеет плавать");
    }
}
class GoldFish extends Animal implements SwimmingAnimal, Fish{
    @Override
    public void swim() {
        System.out.println("Золотая рыбка тоже плывет");
    }
}

class Eagle extends Animal implements FlyingAnimal, Bird {
    @Override
    public void fly() {
        System.out.println("Орел летит быстрее мыши, летучей мыши");
    }
}





