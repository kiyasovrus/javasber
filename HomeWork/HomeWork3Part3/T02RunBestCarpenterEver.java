package HomeWork3Part3;

public class T02RunBestCarpenterEver {
    public static void main(String[] args) {
        Shop shop = new Shop();
        Stools stools = new Stools();
        Table table = new Table();
        System.out.println(shop.serviceable(stools));
        System.out.println(shop.serviceable(table));
    }
}
abstract class Furniture {
}
class Stools extends Furniture{
    public Stools() {
    }
}
class Table extends Furniture{
    public Table() {
    }
}
class Shop {
    public Shop() {
    }
    public boolean serviceable(Furniture furniture){
        return furniture instanceof Stools;
    }
}



