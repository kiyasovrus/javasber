package HomeWorkPM1.T6;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.regex.Pattern;

public class FormValidator {
    private static final Pattern NAME_PATTERN = Pattern.compile("([А-Я][а-я]{1,19})");
    private static final Pattern DATE_PATTERN = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");

    public static void checkName(String str) {
        try {
            if (NAME_PATTERN.matcher(str).matches())
                System.out.println("Имя введено верно!");
            else throw new Exception();

        } catch (Exception ex) {
            System.out.println("Имя введено не верно");
        }
    }

    public static void checkBirthdate(String str) {

        try {
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            df.setLenient(false);
            Date date = df.parse(str);

            String LimitsDate = "01.01.1900";
            Calendar currentDate = Calendar.getInstance();
            Calendar pastLimitsDate = Calendar.getInstance();
            pastLimitsDate.setTime(df.parse(LimitsDate));

            if (date.before(currentDate.getTime())
                    && date.after(pastLimitsDate.getTime())) {
                System.out.println("Дата введена корректно");
            } else throw new Exception();
        } catch (Exception e) {
            System.out.println("Дата введена неверно");

        }
    }

    public static void checkGender(String str) {
        try {
            Gender.valueOf(str);
            System.out.println("Пол введен корректно");
        } catch (IllegalArgumentException e) {
            System.out.println("введенный пол не соответствует шаблону");
        }
    }

    public static void checkHeight(String str) {
        try {
            double d = Double.parseDouble(str);
            if (d > 0)
                System.out.println("рост введен корректно");
            else throw new InputMismatchException();
        } catch (InputMismatchException ex) {
            System.out.println("рост введен не корректно");
        }
    }

    public static void main(String[] args) throws Exception {

        checkName("Иван");
        checkName("bj");
        checkName("кира");

        checkBirthdate("30.13.1991");
        checkBirthdate("30.12.1991");
        checkBirthdate("30.12.2023");
        checkBirthdate("ыва");

        checkGender("Male");
        checkGender("female");

        checkHeight("-1.85");
        checkHeight("2.0");


    }
}
