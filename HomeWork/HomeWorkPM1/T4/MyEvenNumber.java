package HomeWorkPM1.T4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws OddNumberException {
        setN(n);
    }

    public int getN() {
        return n;
    }

    public void setN(int n) throws OddNumberException {
        if (n % 2 == 0)
            this.n = n;
        else throw new OddNumberException();
    }

    public static void main(String[] args) {
        try {
            MyEvenNumber num2 = new MyEvenNumber(10);
            MyEvenNumber num = new MyEvenNumber(5);

        } catch (OddNumberException e) {
            System.out.println(e.getMessage());
        }

    }
}