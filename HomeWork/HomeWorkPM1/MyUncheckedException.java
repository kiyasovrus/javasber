package HomeWorkPM1;

public class MyUncheckedException extends RuntimeException{

    public MyUncheckedException(String message, Throwable cause) {
        super(message, cause);
    }
}
