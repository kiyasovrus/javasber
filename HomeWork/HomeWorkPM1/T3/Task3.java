package HomeWorkPM1.T3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task3 {

    private static final String PATH = "/Users/apple/IdeaProjects/HomeWork/git_test/src/HomeWorkPM1/T3";

    public static void main(String[] args) {
        try {
            inputInUpperCase();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void inputInUpperCase() throws IOException {
        Scanner scanner = new Scanner(new File(PATH + "/" + "input.txt"));
        PrintWriter wr = new PrintWriter(PATH + "/" + "output.txt");

        try (scanner; wr) {
            while (scanner.hasNext()) {
                wr.println(scanner.nextLine().toUpperCase());
            }
        }


    }
}