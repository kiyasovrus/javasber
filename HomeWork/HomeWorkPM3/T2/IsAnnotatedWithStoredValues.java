package HomeWorkPM3.T2;

import HomeWorkPM3.IsLike;

public class IsAnnotatedWithStoredValues {
    public static void IsAnnotated(Class<?> clazz) {
        if (clazz.isAnnotationPresent(IsLike.class)){
            System.out.println(true);
            System.out.println("Value: " + clazz.getAnnotation(IsLike.class).value());
        }
        else {
            System.out.println(false);
        }
    }

}
