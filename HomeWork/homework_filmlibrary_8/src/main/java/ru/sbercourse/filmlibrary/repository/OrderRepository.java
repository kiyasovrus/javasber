package homework_filmlibrary_8.src.main.java.ru.sbercourse.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourse.filmlibrary.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
