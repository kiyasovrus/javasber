package homework_filmlibrary_8.src.main.java.ru.sbercourse.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourse.filmlibrary.model.Director;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
}
