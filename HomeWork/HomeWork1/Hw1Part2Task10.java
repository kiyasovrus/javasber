package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task10 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        double n = console.nextInt();

        double a = Math.log(Math.exp(n));
        //System.out.println(a);
        double b = Math.abs(a) - Math.abs(n);
        //System.out.println(b);
        int y = (int) b;

        if (y == 0) {
           System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}