package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int angle = console.nextInt();

        double a = Math.pow(Math.sin(angle),2);
        double b = Math.pow(Math.cos(angle),2);
        double x = (b + a) - 1;
        int y = (int) x;



        if (y == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}