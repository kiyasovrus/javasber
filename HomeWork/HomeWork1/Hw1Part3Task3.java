package HomeWork1;

import java.util.Scanner;

public class Hw1Part3Task3 {

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int m = console.nextInt();
        int n = console.nextInt();

        int res = 0;
        for (int i = 1; i <= n; i++) {
            res = res + (int)Math.pow(m,i);

        }
        System.out.println(res);
    }
}
