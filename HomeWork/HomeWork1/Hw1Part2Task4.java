package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int dayOfWeek = console.nextInt();

        int remainingDays = 7 - dayOfWeek;
        if (remainingDays == 1 || remainingDays == 0) {
            System.out.println("Ура, выходные!");
        } else {
            System.out.println(remainingDays - 1);
        }
    }
}