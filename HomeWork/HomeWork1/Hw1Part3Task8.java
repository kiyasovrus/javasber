package HomeWork1;

import java.util.Scanner;

/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , ... an
Необходимо вычислить сумму всех чисел a1, a2, a3 ... an которые строго больше p.
 */

public class Hw1Part3Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int p = console.nextInt();

        int sum = 0;

        for (int i = 0; i < n; i++) {
            int k = console.nextInt();

            if (k > p) {
               sum = sum + k;
            }

        }
        System.out.println(sum);
    }
}

