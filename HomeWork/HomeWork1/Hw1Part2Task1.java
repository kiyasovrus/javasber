package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int firstRating = console.nextInt();
        int secondRating = console.nextInt();
        int thirdRating = console.nextInt();

        if (firstRating > secondRating && secondRating > thirdRating) {
            System.out.println("Петя, пора трудиться");
        } else {
            System.out.println("Петя молодец!");
        }

    }
}