package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String sentence = console.nextLine();

        int k = sentence.lastIndexOf(' ');
        String first = sentence.substring(0, k);
        String second = sentence.substring(k + 1);

        System.out.println(first + "\n" + second);
    }
}