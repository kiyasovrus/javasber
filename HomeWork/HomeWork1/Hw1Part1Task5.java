package HomeWork1;

import java.util.Scanner;

public class Hw1Part1Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int inches = console.nextInt();
        double centimetres = inches * 2.54;
        System.out.println(centimetres);
    }
}