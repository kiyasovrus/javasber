package HomeWork1;

import java.util.Scanner;

public class Hw1Part1Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int seconds = console.nextInt();

        int allMinutes = (seconds - (seconds % 60)) / 60;
        int hours = (allMinutes - (allMinutes % 60)) / 60;
        int minutes = allMinutes - (hours * 60);
        System.out.println(hours + " " + minutes);

        //System.out.println(allMinutes);
        //System.out.println(hours);
        //System.out.println(minutes);
    }
}