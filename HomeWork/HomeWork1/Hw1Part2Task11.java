package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task11 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();

        double alpha = Math.toDegrees(Math.acos((Math.pow(a,2) + Math.pow(c,2) - Math.pow(b,2))/(2 * a * c)));
        double beta = Math.toDegrees(Math.acos((Math.pow(a,2) + Math.pow(b,2) - Math.pow(c,2))/(2 * a * b)));
        double gamma = Math.toDegrees(Math.acos((Math.pow(b,2) + Math.pow(c,2) - Math.pow(a,2))/(2 * c * b)));

        //System.out.println(alpha);
        //System.out.println(beta);
        //System.out.println(gamma);

        double x = (180 - (alpha + beta + gamma));
        int y = (int)x;

        if (y == 0 && alpha > 0 && beta > 0 && gamma > 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
