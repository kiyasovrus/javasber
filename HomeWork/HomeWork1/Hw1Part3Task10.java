package HomeWork1;//import java.util.Scanner;
//
//public class Solution {
//
//    public static void main(String[] args) {
//        Scanner console = new Scanner(System.in);
//        int n = console.nextInt();
//
//        for (int i = 1; i <= n; i++) {
//            for (int j = i; j < n; j++) {
//                System.out.print(" ");
//            }
//            for (int j = i; j > 1; j--) {
//                System.out.print("#");
//            }
//            for (int j = 1; j <= i; j++) {
//                System.out.print("#");
//            }
//            System.out.println();
//        }
//    }
//}

// циклы ПОНЯТЬ как-нибудь

import java.util.Scanner;

public class Hw1Part3Task10 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();

        for (int i = 0; i < n; System.out.println(), i++) {
            for (int k = 1; k < n - i; k++)
                System.out.print(' ');
            for (int k = 0; k < 1 + i * 2; k++)
                System.out.print("#");
        }
        for (int i = 1; i < n; i++) {
            System.out.print(" ");
        }
        System.out.println("|");
    }
}