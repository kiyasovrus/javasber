package HomeWork1;

import java.util.Scanner;

public class Hw1Part2Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();


        if ((Math.pow(b,2) - 4 * a * c) < 0) {
            System.out.println("Решения нет");
        } else {
            System.out.println("Решение есть");
        }
    }
}