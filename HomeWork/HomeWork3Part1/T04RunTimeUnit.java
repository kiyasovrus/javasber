package HomeWork3Part1;

public class T04RunTimeUnit {
        public static void main(String[] args) {
            TimeUnit timeUnit = new TimeUnit();
            timeUnit = new TimeUnit(11, 25, 30);
            timeUnit.currentTime();
            timeUnit.currentTime12hoursFormat();
            timeUnit.addTime(12, 20, 0);
            timeUnit.currentTime12hoursFormat();

    }
}

class TimeUnit {

    private int hours, minutes, seconds;

    public TimeUnit() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    public TimeUnit(int hours, int minutes, int seconds) {
        if ((hours < 0 || hours > 23) || (minutes < 0 || minutes > 59) || (seconds < 0 || seconds > 59)) {
            warning();
        } else {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }
    }

    public TimeUnit(int hours, int minutes) {
        if ((hours < 0 || hours > 23) || (minutes < 0 || minutes > 59)) {
            warning();
        } else {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = 0;
        }
    }

    public TimeUnit(int hours) {
        if ((hours < 0 || hours > 23)) {
            warning();
        } else {
            this.hours = hours;
            this.minutes = 0;
            this.seconds = 0;
        }
    }

    public void currentTime() {
        System.out.printf("%02d:%02d:%02d\n", hours, minutes, seconds);
    }

    public void currentTime12hoursFormat() {
        if (hours < 12) {
            System.out.printf("%02d:%02d:%02d am\n", hours, minutes, seconds);
        } else if (hours == 12) {
            System.out.printf("%02d:%02d:%02d pm\n", hours, minutes, seconds);
        } else {
            hours -= 12;
            System.out.printf("%02d:%02d:%02d pm\n", hours, minutes, seconds);
        }

    }

    public void addTime(int hour,int min, int sec) {
        if (hour > 24 || min > 60 || sec > 60)
            System.out.println("неверно введены данные");
        else {
            hours += hour;
            if (hours > 24) {
                hours = hours - 24;
            }
            minutes += min;
            if (minutes > 60) {
                minutes -= 60;
            }
            seconds += sec;
            if (seconds > 60) {
                seconds -= 60;
            }
        }
    }


    private void warning() {
        System.out.println(
                "Incorrect input!\n" +
                        "the HOURS value must be in the range from 0 to 23\n" +
                        "the MINUTES value must be in the range from 0 to 59\n" +
                        "the SECONDS value must be in the range from 0 to 59");
    }

}
