package HomeWork3Part1;

public class T08RunAtm {
    public static void main(String[] args) {
        Atm atm1 = new Atm(250, "usd");
        Atm atm2 = new Atm(12300, "rub");
        System.out.println("Сумма " + atm1.getDollars() + " $" + ", конвертировано в " + atm1.convertDollarsToRubles() + " \u20BD");
        System.out.println("Сумма " + atm2.getRubles() + " \u20BD" + ", конвертировано в " + atm2.convertRublesToDollars() + " $");
        Atm.showCount();
    }
}
class Atm {
    private int dollars;
    private int rubles;

    private static int counter;

    public Atm(int i, String currency) {
        if (currency.equalsIgnoreCase("rub")) {
            this.rubles = i;
            counter++;
        }
        if (currency.equalsIgnoreCase("usd")) {
            this.dollars = i;
            counter++;
        }
    }

    public int getDollars() {
        return dollars;
    }

    public int getRubles() {
        return rubles;
    }
    int usdRubFx = (int) (Math.random()*(500-100+1)+100);
    public int convertDollarsToRubles() {
        return dollars * usdRubFx;
    }

    public int convertRublesToDollars() {
        return rubles / usdRubFx;
    }

    public static void showCount() {
        System.out.println(counter);
    }

}