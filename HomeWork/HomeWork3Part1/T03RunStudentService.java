package HomeWork3Part1;

public class T03RunStudentService {
    public static void main(String[] args) {
        Student bayazitov = new Student();
        Student meldugureev = new Student();
        Student sakhno = new Student();

        bayazitov.setSurname("Баязитов");
        meldugureev.setSurname("Мелдугуреев");
        sakhno.setSurname("Сахно");

        bayazitov.setGrades(new int[]{1,2,3,4,5,6,7,8,9,10});
        meldugureev.setGrades(new int[]{1,2,3,4,5,6,7,8,10,10});
        sakhno.setGrades(new int[]{1,2,3,4,5,6,7,8,10,10});

        Student[] students = new Student[]{bayazitov, meldugureev, sakhno};

        System.out.println(StudentService.bestStudent(students).getSurname());
        System.out.println("----------------");
        StudentService.sortBySurname(students);
        for(Student student : students) {
            System.out.println(student.getSurname());
        }

    }
}

class StudentService {
    private StudentService(){}
    public static Student bestStudent(Student[] students) {
        double maxGrade = -1.0;
        int bestStudentIndex = -1;
        for (int i = 0; i < students.length; i++) {
            if (students[i].averageGrade() > maxGrade) {
                maxGrade = students[i].averageGrade();
                bestStudentIndex = i;
            }
        }
        return students[bestStudentIndex];
    }

    public static Student[] sortBySurname(Student[] students) {
        for (int a = 0; a < students.length - 1; a++) {
            for (int b = a + 1; b < students.length; b++) {
                if (students[a].getSurname().compareTo(students[b].getSurname()) > 0) {
                    String temp = students[a].getSurname();
                    students[a].setSurname(students[b].getSurname());
                    students[b].setSurname(temp);
                }
            }
        }
        return students;
    }
}
