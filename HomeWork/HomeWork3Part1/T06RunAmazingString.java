package HomeWork3Part1;

import java.util.Arrays;

public class T06RunAmazingString {
        public static void main(String[] args) {
            AmazingString s1 = new AmazingString("boilinghead");
            AmazingString s2 = new AmazingString("boilinghead");
            AmazingString s3 = new AmazingString("boilinghead, heat and pain");
            System.out.println(s1.charFromString(s1.charsToString(s1.getChars()), 0));
            System.out.println(s1.lengthString(s1.charsToString(s1.getChars())));
            s1.displayString(s1.getChars());
            System.out.println(s1.checkChars(s2.getChars()));
            System.out.println(s1.checkString("boilinghead"));
            AmazingString.removeSpace(s3.charsToString(s3.getChars()));
            AmazingString.reverseString(s1.charsToString(s1.getChars()));
        }
}
class AmazingString {
    char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String s) {
        this.chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            this.chars[i] = s.charAt(i);
        }
    }

    public char[] getChars() {
        return chars;
    }

    public void setChars(char[] chars) {
        this.chars = chars;
    }

    public String charsToString(char[] arrayChars) {
        return new String(arrayChars);
    }

    public char charFromString(String s, int n) {
        char[] chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
        }
        return chars[n];
    }

    public int lengthString(String s) {
        int size = 0;
        char[] chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
            size++;
        }
        return size;
    }

    public void displayString(char[] arrayChars) {
        String str = new String(arrayChars);
        System.out.println(str);
    }

    public boolean checkChars(char[] array) {
        return Arrays.equals(this.chars, array);
    }

    public boolean checkString(String s) {
        char[] array = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            array[i] = s.charAt(i);
        }
        return Arrays.equals(this.chars, array);
    }

    public static void removeSpace(String s) {
        char[] array = new char[s.length()];

        int size = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                size++;
            }
            array[i] = s.charAt(i);
        }
        char[] arrayWithoutSpaces = new char[s.length() - size];
        int temp = 0;

        for(int i = 0; i < arrayWithoutSpaces.length + size; i++){
            if(array[i] != ' ') {
                arrayWithoutSpaces[temp] = array[i];
                temp++;
            }
        }
        String string = new String(arrayWithoutSpaces);
        System.out.println(string);
    }

    public static void reverseString(String s){
        char[] array = new char[s.length()];
        char[] arrayReverse = new char[s.length()];

        for (int i = 0; i < s.length(); i++) {
            array[i] = s.charAt(i);
        }
        int number = s.length() - 1;
        for (int i = 0; i < array.length; i++) {
            arrayReverse[i] = array[number];
            number--;
        }
        String result = new String(arrayReverse);
        System.out.println(result);
    }
}
