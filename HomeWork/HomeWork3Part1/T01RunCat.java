package HomeWork3Part1;

public class T01RunCat {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
        cat.status();
    }
}

class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }
    private void meow() {
        System.out.println("Meow");
    }
    private void eat() {
        System.out.println("Eat");
    }
    public void status() {
        int n = (int) (Math.random() * 3);
        switch (n) {
            case 0 -> this.sleep();
            case 1 -> this.meow();
            case 2 -> this.eat();

        }
    }
}

