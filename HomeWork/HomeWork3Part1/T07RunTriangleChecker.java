package HomeWork3Part1;

public class T07RunTriangleChecker {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.isATriangle(1.9, 2.3, 30));
        System.out.println(TriangleChecker.isATriangle(3.3, 3, 1));
    }
}
class TriangleChecker {
    private double a, b, c;

    public static boolean isATriangle(double a, double b, double c) {
        return  (a + b) > c && (a + c) > b && (c + b) > a;
    }
}

// проверить еще раз