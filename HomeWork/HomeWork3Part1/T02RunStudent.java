package HomeWork3Part1;

import java.util.Arrays;

public class T02RunStudent {
    public static void main(String[] args) {
        Student kiyasov = new Student();

        kiyasov.setName("Руслан");
        System.out.println(kiyasov.getName());
        kiyasov.setSurname("Киясов");
        System.out.println(kiyasov.getSurname());
        kiyasov.setGrades(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        kiyasov.getGrades();
        kiyasov.addGrade(2);
        kiyasov.getGrades();
        System.out.println(kiyasov.averageGrade());

    }
}

class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void getGrades() {
        System.out.println(Arrays.toString(grades));;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        for (int i = 0; i < grades.length - 1; i++) {
            grades[i] = grades[i + 1];
        }
        grades[grades.length - 1] = grade;
    }

    public double averageGrade() {
        int avrg = 0;
        for (int a : grades) {
            avrg += a;
        }
        return avrg / 10.0;
    }

}



