package HomeWork3Part1;

public class T05RunDayOfWeek {
    public static void main(String[] args) {
        DayOfWeek monday = new DayOfWeek((byte) 1, "Monday");
        DayOfWeek tuesday = new DayOfWeek((byte) 2, "Tuesday");
        DayOfWeek wednesday = new DayOfWeek((byte) 3, "Wednesday");
        DayOfWeek thursday = new DayOfWeek((byte) 4, "Thursday");
        DayOfWeek friday = new DayOfWeek((byte) 5, "Friday");
        DayOfWeek saturday = new DayOfWeek((byte) 6, "Saturday");
        DayOfWeek sunday = new DayOfWeek((byte) 7, "Sunday");

        DayOfWeek[] daysOfWeek = new DayOfWeek[] {
                monday, tuesday, wednesday, thursday, friday, saturday, sunday
        };

        for(DayOfWeek dayOfWeek : daysOfWeek) {
            dayOfWeek.printDayOfWeek();
        }
    }
}
class DayOfWeek {
    private byte dayOfWeekIndex;
    private String dayOfWeekTitle;

    public DayOfWeek(byte dayOfWeekIndex, String dayOfWeekTitle) {
        this.dayOfWeekIndex = dayOfWeekIndex;
        this.dayOfWeekTitle = dayOfWeekTitle;
    }

    public void printDayOfWeek() {
        System.out.println(dayOfWeekIndex + " " + dayOfWeekTitle);
    }

}
