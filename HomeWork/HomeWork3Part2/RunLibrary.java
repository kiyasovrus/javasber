package HomeWork3Part2;

import java.util.ArrayList;

public class RunLibrary {
    public static void main(String[] args) {
//        добавление книг
        Library.LIBRARY.addBook(new Book("Мир, прогресс и права человека", "Андрей Сахаров"));
        Library.LIBRARY.addBook(new Book("Воспоминание о битве при Сольферино", "Анри Дюран"));
        Library.LIBRARY.addBook(new Book("Закон насилия и закон любви", "Лев Толстой"));
        Library.LIBRARY.addBook(new Book("Тихий Дон", "Михаил Шолохов"));

//        книга уже есть в библиотеке
        System.out.println();
        Library.LIBRARY.addBook(new Book("Мир, прогресс и права человека", "Андрей Сахаров"));

//        показать все
        System.out.println();
        Library.LIBRARY.showBooks();

//        удаление книги
        System.out.println();
        Library.LIBRARY.deleteBook("Закон насилия и закон любви");
        Library.LIBRARY.deleteBook("Здравый смысл");

//        поиск по названию
        System.out.println();
        System.out.println(Library.LIBRARY.nameSearch("Здравый смысл"));
        System.out.println(Library.LIBRARY.nameSearch("Приватизация прибыли и национализация убытков"));

//        поиск по автору
        System.out.println();
        System.out.println(Library.LIBRARY.authorSearch("Лондон"));
        System.out.println(Library.LIBRARY.authorSearch("Воннегут"));

//        посетитель
        Visitor visitor1 = new Visitor("Эверейдж Джо Консерватинович");
        Visitor visitor2 = new Visitor("Эверейдж Джо Прогрессинович");
    }
}

class Book {
    private String name;
    private String author;


    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }
}
class Visitor {
    private String name;
    private String identifier;

    public Visitor(String name) {
        this.name = name;
    }

    public Visitor(String name, String identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
class Library {
    public static final Library LIBRARY = new Library();
    private Library() {}
    public ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equals(book.getName())) {
                contains = true;
                break;
            }
        }

        if (contains) {
            System.out.println("Книга \"" + book.getName() + "\" не добавлена, так как она уже есть в библиотеке.");
        } else {
            books.add(book);
            System.out.println("Книга \"" + book.getName() + "\" добавлена в библиотеку.");
        }
    }

    public void deleteBook(String nameBook) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                contains = true;
                books.remove(b);
                break;
            }
        }

        if (contains)
            System.out.println("Книга \"" + nameBook + "\" удалена из библиотеки.");
        else
            System.out.println("Невозможно удалить книгу \"" + nameBook + "\", так как она не найдена в библиотеке.");
    }

    public void showBooks(){
        System.out.println("В библиотеке содержатся следующие книги:");
        for (Book b : books) {
            System.out.println(b.getName() + ", " + b.getAuthor());
        }
    }

    public String nameSearch (String nameBook) {
        String book = null;
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                book = b.getName() + ", " + b.getAuthor();
                break;
            }
        }
        if (book == null)
            return "Книга \"" + nameBook + "\" не найдена.";
        else
            return ("Найдена книга: " + book);
    }

    public String authorSearch (String authorBook) {
        String author = null;
        for (Book b : books) {
            if (b.getAuthor().equalsIgnoreCase(authorBook)) {
                author = b.getName() + ", " + b.getAuthor();
                break;
            }
        }
        if (author == null)
            return "Книга \"" + authorBook + "\" не найдена.";
        else
            return ("Найдена книга: " + author);
    }
}
