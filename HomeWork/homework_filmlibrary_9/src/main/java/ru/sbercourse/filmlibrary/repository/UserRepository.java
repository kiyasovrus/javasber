package homework_filmlibrary_9.src.main.java.ru.sbercourse.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourse.filmlibrary.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {
}
