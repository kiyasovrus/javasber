package homework_filmlibrary_9.src.main.java.ru.sbercourse.filmlibrary.repository;

import org.springframework.stereotype.Repository;
import ru.sbercourse.filmlibrary.model.Film;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
